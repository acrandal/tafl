"""
"""

from enum import Enum


class GameTurnState(Enum):
    ATTACKER = 0
    DEFENDER = 1


class GameTurn(object):
    def __init__(self, starting_player=GameTurnState.ATTACKER):
        self._turn = starting_player

    def go_next(self):
        if self._turn == GameTurnState.ATTACKER:
            self._turn = GameTurnState.DEFENDER
        elif self._turn == GameTurnState.DEFENDER:
            self._turn = GameTurnState.ATTACKER

    def is_attacker(self):
        return self._turn == GameTurnState.ATTACKER

    def is_defender(self):
        return self._turn == GameTurnState.DEFENDER
