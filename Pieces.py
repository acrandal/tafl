"""
"""

from enum import Enum
import logging

from Location import Location


class PieceType(Enum):
    UNKNOWN = 0
    KING = 1
    ATTACKER = 2
    DEFENDER = 3


class Piece(object):
    """ """
    piece_count = 0

    def __init__(self, starting_x=-1, starting_y=-1):
        self.id = Piece.piece_count     # Save my ID
        Piece.piece_count += 1          # Increment for next piece

        self._log = logging.getLogger(name=__class__.__name__ + str(self.id))
        self.symbol = "p"
        self.location = Location(x=starting_x, y=starting_y)
        self.starting_location = Location(x=starting_x, y=starting_y)
        self.captured = False
        self.type = PieceType.UNKNOWN

    def get_terminal_string(self):
        return self.symbol

    def __str__(self):
        return "{0} {1} ({2}) @ {3}".format(__class__.__name__,
                                            self.symbol,
                                            str(self.id),
                                            str(self.location))

    def get_location(self):
        return self.location

    def is_king(self):
        return self.type == PieceType.KING

    def is_enemy(self, other):
        if self.type in [PieceType.KING, PieceType.DEFENDER]:
            return other.type is PieceType.ATTACKER
        elif self.type is PieceType.ATTACKER:
            return other.type in [PieceType.KING, PieceType.DEFENDER]
        else:
            return False        # can this happen?


class King(Piece):
    def __init__(self, starting_x=-1, starting_y=-1):
        super().__init__(starting_x=starting_x, starting_y=starting_y)
        self.symbol = chr(0x2654)           # White king in unicode
        self.type = PieceType.KING


class Attacker(Piece):
    def __init__(self, starting_x=-1, starting_y=-1):
        super().__init__(starting_x=starting_x, starting_y=starting_y)
        self.symbol = chr(0x265F)           # Black pawn in unicode
        self.type = PieceType.ATTACKER


class Defender(Piece):
    def __init__(self, starting_x=-1, starting_y=-1):
        super().__init__(starting_x=starting_x, starting_y=starting_y)
        self.symbol = chr(0x2659)           # White pawn in Unicode
        self.type = PieceType.DEFENDER


class Pieces(object):
    """ Container for all pieces used in play """
    def __init__(self):
        self._pieces = []           # All pieces
        self.king = None
        self.attackers = []
        self.defenders = []
        self.captured = []

    def add_piece(self, new_piece):
        self._pieces.append(new_piece)              # Add to list of all pieces
        if(new_piece.type == PieceType.KING):
            self.king = new_piece
        elif(new_piece.type == PieceType.ATTACKER):
            self.attackers.append(new_piece)
        elif(new_piece.type == PieceType.DEFENDER):
            self.defenders.append(new_piece)

    def get_pieces(self):
        return self._pieces

    def dump_pieces(self):
        for piece in self._pieces:
            print(piece)
