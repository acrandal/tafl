"""
"""

from enum import Enum


class WinState(Enum):
    NoWinner = 1
    Defenders = 2
    Attackers = 3
    Tie = 4
