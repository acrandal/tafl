"""

"""

import logging
# from enum import Enum

from GameTurn import GameTurn
from Exceptions import DiagonalMove, OffBoard, NotYourPiece
from Pieces import PieceType
from WinState import WinState
from Direction import Direction


class GameModel(object):
    def __init__(self, game_settings):
        self._log = logging.getLogger(name=__class__.__name__)  # noqa: F821
        self._log.info("Building Game Model")

        self._turn = GameTurn()
        self._game_settings = game_settings
        self._win_state = WinState.NoWinner

        # Observer pattern interface options
        self.event_types = ["board_update", "turn_change"]
        self.events = {event: dict() for event in self.event_types}

        # Setup the board structure using the game settings' configuration
        self._board = self._game_settings.generate_board()

    def is_game_over(self):
        return self._win_state is not WinState.NoWinner

    def is_game_tie(self):
        return self._win_state is WinState.Tie

    def is_attacker_win(self):
        return self._win_state is WinState.Attackers

    def is_defender_win(self):
        return self._win_state is WinState.Defenders

    def get_turn(self):
        if self._turn.is_attacker():
            return "Attacker ({0})".format(chr(0x265F))
        elif self._turn.is_defender():
            return "Defender ({0})".format(chr(0x2659))

    def dispatch(self, event, dispatcher, message):
        self._log.debug("Site {0} is dispatching {1}".format("model", event))
        for subscriber, callback in self.get_subscribers(event).items():
            callback(dispatcher, message)

    def get_subscribers(self, event):
        return self.events[event]

    def register(self, event, who, callback=None):
        """ Add an observer to a named event """
        if callback is None:
            callback = getattr(who, 'update')
        self.get_subscribers(event)[who] = callback

    def register_board_update(self, who, callback=None):
        self.register("board_update", who, callback)

    def register_turn_change(self, who, callback=None):
        self.register("turn_change", who, callback)

    def unregister(self, event, who):
        del self.get_subscribers(event)[who]

    def unregister_board_update(self, who):
        self.unregister(self, "board_update", who)

    def unregister_turn_change(self, who):
        self.unregister(self, "turn_change", who)

    def board_updated(self):
        """ Called when the board status updates """
        self.dispatch("board_update", self, "board_update")

    def turn_changed(self):
        """ Called when the turn changes """
        self.dispatch("turn_change", self, "turn_change")

    def _is_piece_turn_to_move(self, piece):
        if piece.type is PieceType.ATTACKER and self._turn.is_attacker():
            return True
        elif (piece.type in [PieceType.DEFENDER, PieceType.KING] and
              self._turn.is_defender()):
            return True
        else:
            return False

    def _is_king_captured(self):
        king = self._game_settings.get_king()
        # print("King location: {0}".format(king.get_location()))
        neighbor_locs = []
        for dir in Direction:
            neighbor_locs.append(king.get_location() + dir)
        for loc in neighbor_locs:
            if not self._board.is_location_on_board(loc):   # king board edge
                return False
            if (self._board.is_location_on_board(loc) and
               not self._board.is_cell_hostile(loc, king)):
                return False
        return True

    def _is_king_escaped(self):
        king = self._game_settings.get_king()
        loc = king.get_location()
        if self._board.is_cell_escape(loc):
            self._log.debug("King has escaped")
            return True
        else:
            return False

    def _calc_win_state(self):
        self._log.debug("Calculating if there's a winner")
        if self._is_king_captured():
            self._win_state = WinState.Attackers
        elif self._is_king_escaped():
            self._win_state = WinState.Defenders

    def move_piece(self, location_start, location_end):
        """ Try to move a piece from start to end locations """
        if location_start.x != location_end.x and \
           location_start.y != location_end.y:
            msg = "Invalid move: diagonal: {0} -> {1}".format(
                str(location_start), str(location_end))
            raise DiagonalMove(msg)
        if not self._board.is_location_on_board(location_start):
            raise OffBoard("Start off board: {0}".format(str(location_start)))
        if not self._board.is_location_on_board(location_end):
            raise OffBoard("End off board: {0}".format(str(location_end)))

            # Raises NoPiece if no piece at location_start
        piece = self._board.get_piece_at_location(location_start)

        if not self._is_piece_turn_to_move(piece):
            msg = "Not your piece to move: {0}".format(str(piece))
            raise NotYourPiece(msg)

        self._board.move_piece(location_start, location_end)
        self._calc_win_state()

        self.board_updated()
        self._turn.go_next()
        self.turn_changed()

        self._log.debug(str(piece))

    def get_board_terminal_string(self, indicies=False):
        return self._board.get_terminal_string(indicies=indicies)
