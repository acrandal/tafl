"""
    Trying a board game again...
"""

import logging

from Cell import Cell, CornerCell, ThroneCell
from Direction import Direction
from Exceptions import OffBoard, NoPiece, BlockedMove, SameLocations
from WinState import WinState
# from Pieces import PieceType


class Board(object):
    """ """
    def __init__(self):
        self._log = logging.getLogger(name=__class__.__name__)  # noqa: F821
        self._size = 11                 # Only "standard" sized boards for now
        self._board = []                # 2D array of Cells (tiles)
        self._max_x = self._size - 1
        self._max_y = self._size - 1
        self._win_status = WinState.NoWinner

    def setup(self):
        self._initialize_2D_board()
        self._initialize_cells()
        self._initialize_cell_neighbors()

    def _initialize_2D_board(self):
        # build 2D array to hold cells
        self._board = [[] for _ in range(self._size)]
        for x in range(self._size):
            self._board[x] = [[] for _ in range(self._size)]

    def _initialize_cells(self):
        # Build the cells themselves
        for x in range(self._size):
            for y in range(self._size):
                if((x == 0 and y == 0) or
                   (x == self._max_x and y == 0) or
                   (x == 0 and y == self._max_y) or
                   (x == self._max_x and y == self._max_y)):
                    self._board[x][y] = CornerCell(x=x, y=y)
                elif(x == int(self._size / 2) and y == int(self._size / 2)):
                    self._board[x][y] = ThroneCell(x=x, y=y)
                else:
                    self._board[x][y] = Cell(x=x, y=y)

    def _initialize_cell_neighbors(self):
        # Register neighbors for all cells
        for x in range(self._size):
            for y in range(self._size):
                if(x > 0):                # "West neighbor"
                    self._board[x][y].set_neighbor_cell(self._board[x-1][y],
                                                        Direction.WEST)
                if(y > 0):                # "North neighbor"
                    self._board[x][y].set_neighbor_cell(self._board[x][y-1],
                                                        Direction.NORTH)
                if(x < self._max_x):      # "East neighbor"
                    self._board[x][y].set_neighbor_cell(self._board[x+1][y],
                                                        Direction.EAST)
                if(y < self._max_y):      # "South neighbor"
                    self._board[x][y].set_neighbor_cell(self._board[x][y+1],
                                                        Direction.SOUTH)

    def print_out_cells(self):          # pragma: no cover
        # Dump cell info for debug
        for x in range(self._size):
            for y in range(self._size):
                print(self._board[x][y])

    def place_piece(self, piece):
        x = piece.location.x
        y = piece.location.y
        if not self.is_location_on_board(piece.location):
            raise OffBoard("Piece placed off board: {0}".format(str(piece)))
        self._board[x][y].place_piece(piece)

    def get_piece_at_location(self, location):
        if not self.is_location_on_board(location):
            msg = "Tried to get piece at off board: {0}".format(str(location))
            raise OffBoard(msg)
        piece = self._board[location.x][location.y].get_piece()
        if piece is None:
            raise NoPiece("No piece at location: {0}".format(str(location)))
        return piece

    def is_piece_at_location(self, location):
        try:
            self.get_piece_at_location(location)
        except (OffBoard, NoPiece):
            return False
        return True

    def get_terminal_string(self, indicies=False):  
        # pragma: nocover
        ret = ""
        if indicies is True:
            ret += " X "
            for i in range(self._size):
                ret += "{:<2}".format(i)
            ret += "\n"
        for y in range(self._size):
            if indicies is True:
                if y == 0:
                    ret += "Y "
                else:
                    ret += "  "
            ret += "+-" * self._size + "+\n"
            if indicies is True:
                ret += "{:<2}".format(y)
            for x in range(self._size):
                ret += "|" + self._board[x][y].get_terminal_string()
            ret += "|\n"
        if indicies is True:
            ret += "  "
        ret += "+-" * self._size + "+\n"
        return ret

    def is_location_on_board(self, location):
        if location.x < 0 or location.y < 0 or \
           location.x > self._max_x or \
           location.y > self._max_y:
            return False
        else:
            return True

    def is_cell_hostile(self, loc, piece):
        if not self.is_location_on_board(loc):
            # return False
            raise OffBoard("Cell off board: {0}".format(loc))
        cell = self._get_cell_at_location(loc)
        if cell.is_hostile(piece):
            return True
        if cell.is_full():
            if cell.get_piece().is_enemy(piece):
                return True
        return False

    def is_cell_escape(self, loc):
        if not self.is_location_on_board(loc):
            # return False
            raise OffBoard("Cell off board: {0}".format(loc))
        cell = self._get_cell_at_location(loc)
        return cell.is_escape_cell()

    def _get_cell_at_location(self, loc):
        return self._board[loc.x][loc.y]

    def _is_piece_captured(self, my_loc, new_neigh_loc):
        if self.is_piece_at_location(my_loc):
            myself = self.get_piece_at_location(my_loc)

            if myself.is_king():
                # self._log.debug("KING CHECKING CAPTURE")
                # The king is never "captured" since the game should end
                #  So, for now, he's just invulnerable
                pass
            else:
                dir_to_new = my_loc.calc_direction_to(new_neigh_loc)
                dir_to_opposite = Direction.reverse(dir_to_new)
                opposite_loc = my_loc + dir_to_opposite

                if self.is_piece_at_location(new_neigh_loc) and \
                   self.is_piece_at_location(opposite_loc):
                    piece1 = self.get_piece_at_location(new_neigh_loc)
                    piece2 = self.get_piece_at_location(opposite_loc)
                    if myself.is_enemy(piece1) and myself.is_enemy(piece2):
                        return True
                elif (myself.is_enemy(self.get_piece_at_location(new_neigh_loc)) and        # noqa: E501
                            self.is_location_on_board(opposite_loc) and
                            self._get_cell_at_location(opposite_loc).is_hostile(myself)):   # noqa: E501
                    return True
        return False

    def _handle_successful_move(self, loc_end):
        self._log.debug("Handling a new move to loc: {0}".format(loc_end))
        for dir in Direction:
            check_loc = loc_end + dir
            if not self.is_location_on_board(check_loc):
                continue                        # Don't check off board
            # print(check_loc)
            if self._is_piece_captured(check_loc, loc_end) is True:
                # remove piece
                # print("CAPTURE ME: {0}".format(check_loc))
                self._get_cell_at_location(check_loc).remove_piece()
                # TODO: Something to animate / score / log this?

    def _is_move_valid(self, location_start, location_end):
        direction = location_start.calc_direction_to(location_end)
        piece = self._get_cell_at_location(location_start).get_piece()
        curr_loc = location_start + direction

        # Loop stops if any cell or board flag is raised to stop the move
        #  OR if you reach your destination
        while self.is_location_on_board(curr_loc) is True and \
              self._get_cell_at_location(curr_loc).is_empty() and \
              curr_loc != location_end:                          # noqa: E127
            self._log.debug(str(curr_loc))
            curr_loc = curr_loc + direction
        self._log.debug(str(curr_loc))

        if not self.is_location_on_board(curr_loc):     # off board!
            return False
        end_cell = self._get_cell_at_location(curr_loc)
        if not end_cell.is_piece_placeable(piece):      # any reason no?
            return False
        return True

    def move_piece(self, location_start, location_end):
        self._log.debug("Moving piece attempt: {0} -> {1}".format(
            location_start, location_end))
        piece = self.get_piece_at_location(location_start)

        if location_start == location_end:
            msg = "Both locations same, no move!: {0} == {1}".format(
                location_start, location_end)
            raise SameLocations(msg)
        else:
            if self._is_move_valid(location_start, location_end):
                self._log.debug("Valid move, moving!")
                start_cell = self._get_cell_at_location(location_start)
                start_cell.remove_piece()
                end_cell = self._get_cell_at_location(location_end)
                end_cell.place_piece(piece)
                piece.location = location_end
                self._handle_successful_move(location_end)  # Resolve board
            else:
                msg = "Move blocked: {0} -> {1}".format(
                    location_start, location_end)
                raise BlockedMove(msg)
