""" A single cell on the board (tile, location, square)
"""

import logging

from Exceptions import CellAlreadyFull
from Location import Location
from Pieces import PieceType

from enum import Enum


class CellType(Enum):
    STANDARD = "standard"
    THRONE = "throne"
    CORNER = "corner"


class Cell(object):
    """ A single board cell (tile, square, location) """
    def __init__(self, x=-1, y=-1):
        logName = "{0}({1},{2})".format(__class__.__name__, str(x), str(y))  # noqa: F821,E501
        self._log = logging.getLogger(name=logName)
        self.coords = Location(x=x, y=y)
        self._neighbors = {}
        self._curr_piece = None
        self.symbol = " "
        self.type = CellType.STANDARD

        # self._log.debug("Created cell: " + str(self.coords))

    def __str__(self):
        ret = "C ({0}): {1} -- Neighbors: ".format(self.symbol, self.coords)
        for direction, neighbor in self._neighbors.items():
            ret += "[{0} {1}] | ".format(direction, neighbor.coords)
        return ret

    def set_neighbor_cell(self, neighbor, direction):
        self._neighbors[direction] = neighbor

    def is_full(self):
        if self._curr_piece is not None:
            return True
        return False

    def is_empty(self):
        return not self.is_full()

    def get_piece(self):
        return self._curr_piece

    def get_terminal_string(self):
        if self._curr_piece is None:
            return self.symbol
        return self._curr_piece.get_terminal_string()

    def place_piece(self, new_piece):
        if(self._curr_piece is not None):
            msg = "Cell already full! "
            msg += "Cannot place piece at ({0}, {1})".format(
                self.coords.x, self.coords.y)
            raise CellAlreadyFull(msg)
        else:
            self._curr_piece = new_piece

    def remove_piece(self):
        self._curr_piece = None

    def is_piece_placeable(self, piece):
        """ May a piece be placed on this cell?
            Expected to be overriden by special cells for kings """
        if self.is_empty():
            return True

    def is_hostile(self, piece):
        """ Does this cell "capture" pieces as if it's an enemy? """
        return False

    def is_escape_cell(self):
        """ If this is a cell the King 'escapes' and wins in """
        return False


class CornerCell(Cell):
    """ """
    def __init__(self, x=-1, y=-1):
        super().__init__(x=x, y=y)
        self.symbol = chr(0x16EF)
        self.type = CellType.CORNER

    def is_piece_placeable(self, piece):
        if piece.type is not PieceType.KING:
            return False
        return super().is_piece_placeable(piece)

    def is_hostile(self, piece):
        return True

    def is_escape_cell(self):
        return True


class ThroneCell(Cell):
    """ """
    def __init__(self, x=-1, y=-1):
        super().__init__(x=x, y=y)
        self.symbol = chr(0x2441)
        self.type = CellType.THRONE

    def is_piece_placeable(self, piece):
        if piece.type is not PieceType.KING:
            return False
        return super().is_piece_placeable(piece)

    def is_hostile(self, piece):
        if self.is_full():
            if self._curr_piece.type is PieceType.KING:
                if piece.type is PieceType.DEFENDER:
                    return False
        return True
