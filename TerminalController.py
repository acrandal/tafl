"""
"""

import logging

# from GameModel import GameModel
from Location import Location


class TerminalController(object):
    def __init__(self, model):
        self._log = logging.getLogger(name=__class__.__name__)
        self._log.debug("Creating Controller")

        self._model = model

    def run(self):
        curr_input = ""
        while curr_input != "q":
            curr_input = input()
            # print("user inputted: {0}".format(curr_input))
            if "," in curr_input:
                vals = curr_input.split(",")
                print(vals)
                if len(vals) == 4:
                    x1 = int(vals[0])
                    y1 = int(vals[1])
                    x2 = int(vals[2])
                    y2 = int(vals[3])
                    start_loc = Location(x=x1, y=y1)
                    end_loc = Location(x=x2, y=y2)
                    self._model.move_piece(start_loc, end_loc)
