"""
"""

import logging

# from GameModel import GameModel


class TerminalView(object):
    def __init__(self, model):
        self._log = logging.getLogger(name=__class__.__name__)
        self._log.debug("Creating View")

        self._model = model

        self._register_callbacks()

    def _register_callbacks(self):
        self._model.register_board_update(self, self.handle_board_change)
        self._model.register_turn_change(self, self.handle_turn_change)

    def draw_prompt(self):
        print("Whose turn? -- {0}".format(self._model.get_turn()))
        print("Enter next move x1,y1,x2,y2: ")

    def draw_screen(self):
        print(self._model.get_board_terminal_string(indicies=True))
        self.draw_prompt()

    def handle_board_change(self, who, message):
        self._log.debug("Told the board changed: {0}".format(message))
        # print(who.get_board_terminal_string(indicies=True))
        self.draw_screen()

    def handle_turn_change(self, who, message):
        self._log.debug("Told the turn changed: {0}".format(message))
        # print(who.get_board_terminal_string(indicies=True))
        self.draw_screen()
