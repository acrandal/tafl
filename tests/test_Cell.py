"""
"""

import unittest
from Location import Location
from Exceptions import CellAlreadyFull
from Cell import Cell, ThroneCell, CornerCell, CellType
from Pieces import Attacker, Defender, King
from Direction import Direction


class TestCell(unittest.TestCase):
    def setUp(self):
        self.cell = Cell(x=5, y=10)
        self.default_loc = Location(x=5, y=10)
        self.default_piece = Attacker()

    def test_default_coords(self):
        self.assertEqual(self.cell.coords, self.default_loc)

    def test_place_piece(self):
        self.cell.place_piece(self.default_piece)
        self.assertTrue(self.cell.is_full())

    def test_piece_exception(self):
        cell = Cell(x=5, y=10)
        cell.place_piece(self.default_piece)
        self.assertRaises(CellAlreadyFull,
                          cell.place_piece, self.default_piece)

    def test_is_hostile_attacker(self):
        piece = Attacker()
        self.assertFalse(self.cell.is_hostile(piece))

    def test_is_hostile_defender(self):
        piece = Defender()
        self.assertFalse(self.cell.is_hostile(piece))

    def test_is_hostile_king(self):
        piece = King()
        self.assertFalse(self.cell.is_hostile(piece))

    def test_is_escape_cell(self):
        self.assertFalse(self.cell.is_escape_cell())

    def test_to_str(self):
        self.cell.set_neighbor_cell(self.cell, Direction.NORTH)
        str(self.cell)
        self.assertTrue(True)


class TestCornerCell(unittest.TestCase):
    def setUp(self):
        self.default_loc = Location(x=5, y=10)
        self.cell = CornerCell(x=self.default_loc.x, y=self.default_loc.y)

    def test_corner_cell_terminal(self):
        corner = CornerCell()
        # self.assertEqual(corner.symbol, "x")
        self.assertEqual(corner.symbol, chr(0x16EF))

    def test_corner_cell_type(self):
        corner = CornerCell()
        self.assertEqual(corner.type, CellType.CORNER)

    def test_is_hostile_attacker(self):
        piece = Attacker()
        self.assertTrue(self.cell.is_hostile(piece))

    def test_is_hostile_defender(self):
        piece = Defender()
        self.assertTrue(self.cell.is_hostile(piece))

    def test_is_hostile_king(self):
        piece = King()
        self.assertTrue(self.cell.is_hostile(piece))

   # def test_is_escape_cell(self):
   #     self.assertTrue(self.cell.is_escape_cell())

class TestThroneCell(unittest.TestCase):
    def setUp(self):
        self.default_loc = Location(x=5, y=5)
        self.cell = ThroneCell(x=self.default_loc.x, y=self.default_loc.y)

    def test_throne_cell_terminal(self):
        # throne = ThroneCell()
        # self.assertEqual(throne.symbol, "T")
        self.assertEqual(self.cell.symbol, chr(0x2441))

    def test_throne_cell_type(self):
        # throne = ThroneCell()
        self.assertEqual(self.cell.type, CellType.THRONE)

    def test_is_hostile_attacker(self):
        piece = Attacker()
        self.cell.remove_piece()
        self.assertTrue(self.cell.is_hostile(piece))

    def test_is_hostile_defender(self):
        piece = Defender()
        self.cell.remove_piece()
        self.assertTrue(self.cell.is_hostile(piece))

    def test_is_hostile_king(self):
        piece = King()
        self.cell.remove_piece()
        self.assertTrue(self.cell.is_hostile(piece))

    def test_is_hostile_defender_when_occupied(self):
        piece = Defender()
        king = King()
        self.cell.remove_piece()
        self.cell.place_piece(king)
        self.assertFalse(self.cell.is_hostile(piece))

    def test_is_hostile_attacker_when_occupied(self):
        piece = Attacker()
        king = King()
        self.cell.remove_piece()
        self.cell.place_piece(king)
        self.assertTrue(self.cell.is_hostile(piece))

    def test_is_escape_cell(self):
        self.assertFalse(self.cell.is_escape_cell())