"""
"""

import unittest

from GameTurn import GameTurn, GameTurnState


class TestGameTurn(unittest.TestCase):

    def test_default_turn(self):
        turn = GameTurn()
        self.assertTrue(turn.is_attacker())

    def test_not_default_turn(self):
        turn = GameTurn()
        self.assertFalse(turn.is_defender())

    def test_default_defender_set(self):
        turn = GameTurn(starting_player=GameTurnState.DEFENDER)
        self.assertTrue(turn.is_defender())

    def test_change_turn_to_defender(self):
        turn = GameTurn()
        turn.go_next()
        self.assertTrue(turn.is_defender())

    def test_change_turn_to_attacker(self):
        turn = GameTurn(starting_player=GameTurnState.DEFENDER)
        turn.go_next()
        self.assertTrue(turn.is_attacker())
