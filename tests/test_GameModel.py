"""
"""

import unittest

# from GameTurn import GameTurn, GameTurnState
from GameType import GameTypeTest, GameTypeBlankBoard
from Location import Location
from Pieces import Attacker, King, Defender
from GameModel import GameModel


class TestGameModelWinStates(unittest.TestCase):
    def test_capture_king_throne(self):
        model = GameModel(GameTypeTest())
        model._board.place_piece(Attacker(starting_x=5, starting_y=4))
        model._board.place_piece(Attacker(starting_x=6, starting_y=5))
        model._board.place_piece(Attacker(starting_x=5, starting_y=6))
        model._board.place_piece(Attacker(starting_x=3, starting_y=5))
        piece1_start = Location(x=3, y=5)
        piece1_end = Location(x=4, y=5)
        model.move_piece(piece1_start, piece1_end)
        # print("\n" + model.get_board_terminal_string(indicies=True))
        self.assertTrue(model.is_attacker_win())

    def test_capture_king_against_throne(self):
        model = GameModel(GameTypeBlankBoard())
        king = King(starting_x=5, starting_y=4)
        model._game_settings._pieces.king = king
        model._board.place_piece(king)
        model._board.place_piece(Attacker(starting_x=5, starting_y=2))
        model._board.place_piece(Attacker(starting_x=4, starting_y=4))
        model._board.place_piece(Attacker(starting_x=6, starting_y=4))
        print("\n" + model.get_board_terminal_string(indicies=True))
        piece1_start = Location(x=5, y=2)
        piece1_end = Location(x=5, y=3)
        model.move_piece(piece1_start, piece1_end)      # should capture king
        print("\n" + model.get_board_terminal_string(indicies=True))
        self.assertTrue(model.is_attacker_win())

    def test_capture_king_fail_against_throne(self):
        model = GameModel(GameTypeBlankBoard())
        king = King(starting_x=5, starting_y=4)
        model._game_settings._pieces.king = king
        model._board.place_piece(king)
        model._board.place_piece(Attacker(starting_x=5, starting_y=2))
        model._board.place_piece(Attacker(starting_x=4, starting_y=4))
        model._board.place_piece(Defender(starting_x=6, starting_y=4))
        piece1_start = Location(x=5, y=2)
        piece1_end = Location(x=5, y=3)
        model.move_piece(piece1_start, piece1_end)      # should capture king
        # print("\n" + model.get_board_terminal_string(indicies=True))
        self.assertFalse(model.is_attacker_win())

    def test_capture_king_fail_against_edge(self):
        model = GameModel(GameTypeBlankBoard())
        king = King(starting_x=10, starting_y=3)
        model._game_settings._pieces.king = king
        model._board.place_piece(king)
        model._board.place_piece(Attacker(starting_x=10, starting_y=2))
        model._board.place_piece(Attacker(starting_x=10, starting_y=4))
        model._board.place_piece(Attacker(starting_x=8, starting_y=3))
        piece1_start = Location(x=8, y=3)
        piece1_end = Location(x=9, y=3)
        model.move_piece(piece1_start, piece1_end)      # should capture king
        # print("\n" + model.get_board_terminal_string(indicies=True))
        self.assertFalse(model.is_attacker_win())

    def test_defender_win_upper_right(self):
        model = GameModel(GameTypeBlankBoard())
        king = King(starting_x=10, starting_y=3)
        model._game_settings._pieces.king = king
        model._board.place_piece(king)
        model._turn.go_next()                           # force turn change
        piece1_start = Location(x=10, y=3)
        piece1_end = Location(x=10, y=0)
        model.move_piece(piece1_start, piece1_end)      # should capture king
        # print("\n" + model.get_board_terminal_string(indicies=True))
        self.assertTrue(model.is_defender_win())

