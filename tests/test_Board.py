"""
"""

import unittest
from Location import Location
from Direction import Direction
from GameType import GameTypeStandard, GameTypeTest
from Pieces import Attacker
from Exceptions import BlockedMove, OffBoard, NoPiece, SameLocations


class TestStandardBoard(unittest.TestCase):
    def setUp(self):
        self.game_settings = GameTypeStandard()
        self.board = self.game_settings.generate_board()
        # print("\nSetting up board for testing")
        # print(self.board.get_terminal_string())

    def test_place_piece(self):
        loc = Location(x=1, y=1)
        piece = Attacker(starting_x=loc.x, starting_y=loc.y)
        self.board.place_piece(piece)
        ret_piece = self.board.get_piece_at_location(loc)
        self.assertEqual(piece, ret_piece)

    def test_place_piece_off_board(self):
        loc = Location(x=-1, y=1)
        piece = Attacker(starting_x=loc.x, starting_y=loc.y)
        # Exception, Function, parameters a, b, c, d
        self.assertRaises(OffBoard, self.board.place_piece, piece)

    def test_is_location_on_board_low_x(self):
        loc_low_x = Location(x=-1, y=1)
        self.assertFalse(self.board.is_location_on_board(loc_low_x))

    def test_is_location_on_board_low_y(self):
        loc_low_y = Location(x=1, y=-1)
        self.assertFalse(self.board.is_location_on_board(loc_low_y))

    def test_is_location_on_board_high_x(self):
        loc_high_x = Location(x=11, y=5)
        self.assertFalse(self.board.is_location_on_board(loc_high_x))

    def test_is_location_on_board_high_y(self):
        loc_high_y = Location(x=5, y=11)
        self.assertFalse(self.board.is_location_on_board(loc_high_y))

    def test_is_location_on_board_upper_left(self):
        loc = Location(x=0, y=0)
        self.assertTrue(self.board.is_location_on_board(loc))

    def test_is_location_on_board_lower_right(self):
        loc = Location(x=10, y=10)
        self.assertTrue(self.board.is_location_on_board(loc))

    def test_move_no_piece(self):
        loc = Location(x=9, y=9)
        # Exception, Function, parameters a, b, c, d
        self.assertRaises(NoPiece, self.board.get_piece_at_location, loc)


class TestMoving(unittest.TestCase):
    def setUp(self):
        # board = GameTypeTest().generate_board()
        # print("\n" + board.get_terminal_string(indicies=True))
        pass

    def test_basic_move(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=9, y=6)
        loc_end = Location(x=9, y=5)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_end)
        piece2 = board.get_piece_at_location(loc_end)
        self.assertEqual(piece, piece2)

    def test_move_off_board(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=9, y=6)
        loc_end = Location(x=12, y=6)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_move_collide_piece(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=4, y=9)
        loc_end = Location(x=0, y=9)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_move_collide_corner(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=8, y=0)
        loc_end = Location(x=0, y=0)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_move_def_collide_att(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=9, y=2)
        loc_end = Location(x=9, y=6)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_move_def_collide_def(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=8, y=0)
        loc_end = Location(x=8, y=3)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_move_att_collide_def(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=9, y=10)
        loc_end = Location(x=9, y=7)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_move_att_collide_att(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=8, y=7)
        loc_end = Location(x=1, y=7)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_move_north(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=5, y=5)
        loc_end = Location(x=5, y=0)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_end)
        piece2 = board.get_piece_at_location(loc_end)
        self.assertEqual(piece, piece2)

    def test_move_south(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=5, y=5)
        loc_end = Location(x=5, y=9)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_end)
        piece2 = board.get_piece_at_location(loc_end)
        self.assertEqual(piece, piece2)

    def test_move_east(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=5, y=5)
        loc_end = Location(x=10, y=5)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_end)
        piece2 = board.get_piece_at_location(loc_end)
        self.assertEqual(piece, piece2)

    def test_move_west(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=5, y=5)
        loc_end = Location(x=0, y=5)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_end)
        piece2 = board.get_piece_at_location(loc_end)
        self.assertEqual(piece, piece2)

    def test_king_move_to_throne(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=5, y=5)
        loc_end = Location(x=2, y=5)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_end)            # King off throne
        board.move_piece(loc_end, loc_start)            # King on throne
        piece2 = board.get_piece_at_location(loc_start)
        self.assertEqual(piece, piece2)

    def test_king_move_to_corner(self):
        board = GameTypeTest().generate_board()
        loc_start = Location(x=5, y=5)
        loc_first = Location(x=5, y=0)
        loc_end = Location(x=0, y=0)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_first)            # King off throne
        board.move_piece(loc_first, loc_end)              # King to corner
        piece2 = board.get_piece_at_location(loc_end)
        self.assertEqual(piece, piece2)

    def test_att_move_to_throne(self):
        board = GameTypeTest().generate_board()
        king_start = Location(x=5, y=5)
        king_end = Location(x=5, y=0)
        board.move_piece(king_start, king_end)            # King off throne
        loc_start = Location(x=5, y=10)
        loc_end = Location(x=5, y=5)
        self.assertRaises(BlockedMove, board.move_piece, loc_start, loc_end)

    def test_att_move_over_throne(self):
        board = GameTypeTest().generate_board()
        king_start = Location(x=5, y=5)
        king_end = Location(x=5, y=0)
        board.move_piece(king_start, king_end)            # King off throne
        loc_start = Location(x=5, y=10)
        loc_end = Location(x=5, y=3)
        piece = board.get_piece_at_location(loc_start)
        board.move_piece(loc_start, loc_end)              # Att over throne
        piece2 = board.get_piece_at_location(loc_end)
        self.assertEqual(piece, piece2)

    def test_move_locations_equal_raise(self):
        board = GameTypeTest().generate_board()
        loc = Location(x=5, y=5)
        self.assertRaises(SameLocations, board.move_piece, loc, loc)


class TestCapturing(unittest.TestCase):
    def test_basic_capture(self):
        board = GameTypeTest().generate_board()
        mover_start = Location(x=9, y=2)
        mover_end = Location(x=9, y=5)
        removed_loc = Location(x=9, y=6)
        board.move_piece(mover_start, mover_end)
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertFalse(board.is_piece_at_location(removed_loc))

    def test_double_L_capture(self):
        board = GameTypeTest().generate_board()
        mover_start = Location(x=9, y=10)
        mover_end = Location(x=9, y=8)
        removed_loc1 = Location(x=9, y=7)
        removed_loc2 = Location(x=8, y=8)
        board.move_piece(mover_start, mover_end)
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertFalse(board.is_piece_at_location(removed_loc1) or
                         board.is_piece_at_location(removed_loc2))

    def test_double_Line_capture(self):
        board = GameTypeTest().generate_board()
        mover_start = Location(x=5, y=5)
        mover_end = Location(x=5, y=1)
        removed_loc1 = Location(x=4, y=1)
        removed_loc2 = Location(x=6, y=1)
        board.move_piece(mover_start, mover_end)
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertFalse(board.is_piece_at_location(removed_loc1) or
                         board.is_piece_at_location(removed_loc2))

    def test_corner_capture(self):
        board = GameTypeTest().generate_board()
        mover_start = Location(x=8, y=8)
        mover_end = Location(x=8, y=10)
        removed_loc1 = Location(x=9, y=10)
        board.move_piece(mover_start, mover_end)
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertFalse(board.is_piece_at_location(removed_loc1))

    def test_throne_attacker_capture(self):
        board = GameTypeTest().generate_board()
        king_start = Location(x=5, y=5)
        king_end = Location(x=5, y=0)
        board.move_piece(king_start, king_end)
        piece1_start = Location(x=4, y=1)
        piece1_end = Location(x=4, y=5)
        board.move_piece(piece1_start, piece1_end)
        piece2_start = Location(x=3, y=1)
        piece2_end = Location(x=3, y=5)
        board.move_piece(piece2_start, piece2_end)
        removed_loc = piece1_end
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertFalse(board.is_piece_at_location(removed_loc))

    def test_throne_defender_capture(self):
        board = GameTypeTest().generate_board()
        king_start = Location(x=5, y=5)
        king_end = Location(x=5, y=0)
        board.move_piece(king_start, king_end)
        piece1_start = Location(x=6, y=6)
        piece1_end = Location(x=6, y=5)
        board.move_piece(piece1_start, piece1_end)
        piece2_start = Location(x=7, y=8)
        piece2_end = Location(x=7, y=5)
        board.move_piece(piece2_start, piece2_end)
        removed_loc = piece1_end
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertFalse(board.is_piece_at_location(removed_loc))

    def test_fail_king_capture(self):
        board = GameTypeTest().generate_board()
        piece1_start = Location(x=5, y=10)
        piece1_end = Location(x=5, y=6)
        board.move_piece(piece1_start, piece1_end)
        piece2_start = Location(x=0, y=4)
        piece2_end = Location(x=5, y=4)
        board.move_piece(piece2_start, piece2_end)
        removed_loc = Location(x=5, y=5)
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertTrue(board.is_piece_at_location(removed_loc))

    # Testing no self-captures?
    def test_fail_self_capture(self):
        board = GameTypeTest().generate_board()
        piece1_start = Location(x=8, y=3)
        piece1_end = Location(x=0, y=3)
        board.move_piece(piece1_start, piece1_end)
        removed_loc = piece1_end
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertTrue(board.is_piece_at_location(removed_loc))

    def test_fail_own_team_capture_corner(self):
        board = GameTypeTest().generate_board()
        piece1_start = Location(x=5, y=10)
        piece1_end = Location(x=8, y=10)
        board.move_piece(piece1_start, piece1_end)
        removed_loc = piece1_end + Direction.EAST
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertTrue(board.is_piece_at_location(removed_loc))

    def test_fail_own_team_capture(self):
        board = GameTypeTest().generate_board()
        piece1_start = Location(x=7, y=8)
        piece1_mid = Location(x=5, y=8)
        piece1_end = Location(x=5, y=9)
        board.move_piece(piece1_start, piece1_mid)
        board.move_piece(piece1_mid, piece1_end)
        removed_loc = piece1_end
        # print("\n" + board.get_terminal_string(indicies=True))
        self.assertTrue(board.is_piece_at_location(removed_loc))

    def test_cell_hostile_off_board_raise(self):
        board = GameTypeTest().generate_board()
        loc_offboard = Location(x=20, y=50)
        self.assertRaises(OffBoard, board.is_cell_hostile, loc_offboard, None)

    def test_cell_escape_off_board_raise(self):
        board = GameTypeTest().generate_board()
        loc_offboard = Location(x=20, y=50)
        self.assertRaises(OffBoard, board.is_cell_escape, loc_offboard)
