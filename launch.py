

import logging
# from Cell import Cell
# from Board import Board
# from Pieces import Pieces

# from GameType import GameTypeStandard, GameTypeTest
from GameType import GameTypeTest
from GameModel import GameModel
# from Location import Location
from View import TerminalView
from TerminalController import TerminalController


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    logging.info("Starting up")

    game_settings = GameTypeTest()
    model = GameModel(game_settings)

    print(model._board.get_terminal_string(indicies=True))

    # model.move_piece(Location(x=3, y=0), Location(x=1, y=0))
    # model.move_piece(Location(x=1, y=0), Location(x=1, y=3))
    # model.move_piece(Location(x=5, y=5), Location(x=5, y=2))

    # print(model._board.get_terminal_string(indicies=True))

    view = TerminalView(model)
    view.draw_screen()
    # model.move_piece(Location(x=1, y=3), Location(x=1, y=1))
    controller = TerminalController(model)
    controller.run()

    logging.info("Exiting")
