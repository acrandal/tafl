"""
"""

import logging
from Pieces import Pieces, King, Attacker, Defender
from Board import Board


class GameType(object):
    def __init__(self):
        self._log = logging.getLogger(name=__class__.__name__)
        self._board_size = -1
        self._pieces = Pieces()

    def get_king(self):
        return self._pieces.king


class GameTypeStandard(GameType):
    def __init__(self):
        super().__init__()
        self._log.debug("Creating a standard game setting")
        self.set_defaults()

    def set_defaults(self):
        self._board_size = 11
        self._build_pieces()

    def _build_pieces(self):
        self._pieces.add_piece(King(starting_x=5, starting_y=5))

        defender_locations = [ (5,3), (4,4), (5,4), (6,4),    # noqa: E201,E231
                               (3,5), (4,5), (6,5), (7,5),    # noqa: E201,E231
                               (4,6), (5,6), (6,6), (5,7)]    # noqa: E201,E231
        for coords in defender_locations:
            self._pieces.add_piece(Defender(starting_x=coords[0],
                                            starting_y=coords[1]))

        attacker_locations = []
        for i in range(5):
            attacker_locations.append((3+i, 0))
            attacker_locations.append((3+i, 10))
            attacker_locations.append((0, 3+i))
            attacker_locations.append((10, 3+i))

        for coords in attacker_locations:
            self._pieces.add_piece(Attacker(starting_x=coords[0],
                                            starting_y=coords[1]))

        self._pieces.add_piece(Attacker(starting_x=5, starting_y=1))
        self._pieces.add_piece(Attacker(starting_x=1, starting_y=5))
        self._pieces.add_piece(Attacker(starting_x=9, starting_y=5))
        self._pieces.add_piece(Attacker(starting_x=5, starting_y=9))

    def generate_board(self):
        board = Board()
        board.setup()

        for piece in self._pieces.get_pieces():
            board.place_piece(piece)
        return board

    def print_config(self):             # pragma: no cover
        print("Board size: {0}".format(self._board_size))
        self._pieces.dump_pieces()


class GameTypeTest(GameType):
    def __init__(self):
        super().__init__()
        self._log.debug("Creating a testing game setting")
        self.set_defaults()

    def set_defaults(self):
        self._board_size = 11
        self._build_pieces()

    def _build_pieces(self):
        self._pieces.add_piece(King(starting_x=5, starting_y=5))

        defender_locations = []
        defender_locations = [(3, 1), (7, 1), (9, 2), (8, 3),    # noqa: E201
                              (8, 0), (6, 4), (8, 8), (9, 7),    # noqa: E201
                              (6, 6), (0, 9)]              # noqa: E201 E202

        for coords in defender_locations:
            self._pieces.add_piece(Defender(starting_x=coords[0],
                                            starting_y=coords[1]))

        attacker_locations = []
        attacker_locations = [(4, 1), (6, 1), (8, 7), (7, 8),
                              (9, 10), (9, 6), (5, 10), (4, 9),
                              (6, 9), (0, 4), (0, 6), (1, 7), (0, 2)]

        for coords in attacker_locations:
            self._pieces.add_piece(Attacker(starting_x=coords[0],
                                            starting_y=coords[1]))

    def generate_board(self):
        board = Board()
        board.setup()

        for piece in self._pieces.get_pieces():
            board.place_piece(piece)
        return board

    def print_config(self):             # pragma: no cover
        print("Board size: {0}".format(self._board_size))
        self._pieces.dump_pieces()


class GameTypeBlankBoard(GameType):
    def __init__(self):
        super().__init__()
        self._log.debug("Creating a testing game setting")
        self.set_defaults()

    def set_defaults(self):
        self._board_size = 11

    def generate_board(self):
        board = Board()
        board.setup()

        # for piece in self._pieces.get_pieces():
        #    board.place_piece(piece)
        return board

    def print_config(self):             # pragma: no cover
        print("Board size: {0}".format(self._board_size))
        self._pieces.dump_pieces()
