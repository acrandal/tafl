#
#  Scripts to build, test, and view results of Hnefeltafl
#
#  @contributors:
#   Aaron S. Crandall <acrandal@gmail.com> - 2019
#


build:
	@echo "Nothing to be done for build"

run:
	python3 launch.py

lint:
	flake8 --count --show-source

test:
	python3 -m unittest discover ./tests/ "test_*.py"

vtest:
	python3 -m unittest discover -v ./tests/ "test_*.py"

coverage:
	coverage run -m unittest discover ./tests/ "test_*.py"
	coverage report -m

htmlcov: coverage
	coverage html

viewcoverage: htmlcov
	x-www-browser htmlcov/index.html

clean:
	rm -f *.pyc
	rm -rf htmlcov
