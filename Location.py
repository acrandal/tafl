
from Direction import Direction
from Exceptions import SameLocations


class Location(object):
    def __init__(self, x=-1, y=-1):
        self.x = x
        self.y = y

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)

    def __add__(self, dir):
        if dir == Direction.NORTH:
            return Location(x=self.x, y=self.y - 1)
        elif dir == Direction.SOUTH:
            return Location(x=self.x, y=self.y + 1)
        elif dir == Direction.EAST:
            return Location(x=self.x + 1, y=self.y)
        elif dir == Direction.WEST:
            return Location(x=self.x - 1, y=self.y)

    def __eq__(self, other):
        return (self.x == other.x and self.y == other.y)

    def calc_direction_to(self, other):
        if self.x < other.x:
            return Direction.EAST
        elif self.x > other.x:
            return Direction.WEST
        elif self.y < other.y:
            return Direction.SOUTH
        elif self.y > other.y:
            return Direction.NORTH
        raise SameLocations("Locations equal")
